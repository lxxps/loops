**Loops\Autoloader**
===========



Requirements:
-------------

- At least PHP 5.3.



How to use it:
--------------


### Autoloading

If you do not have composer, call bootstrap:

    :::php
      require $path_to_package.'/bootstrap.inc.php';

*This library use package-oriented autoloading with a directory for package 
namespace (PSR-4) and underscore as directory separator for class names (PSR-0).*


### PSR-4/PSR-0 mixin

Purpose of the PSR-4/PSR-0 mixin autoloading, is to have a package-oriented 
autoloading with a unique namespace `Vendor/Package` for the entire package.

In order to keep a usable structure, all classes for this package uses 
underscores as directory separators.

    :::php
      // register PSR-4/PSR-0 mixin autoloading for the package
      \Loops\Autoloader\Psr40::getInstance()->add( 'Vendor\\Package' , $path_to_package_root.'/src' );

      $o = new \Vendor\Package\My_ClassName();
      // look for $path_to_package_root.'/src/My/ClassName.php'


### Register a namespace

The \Loops\Autoloader\Psr40 class has been designed has singleton to handle 
*special* package-oriented autoloading.

    :::php
      // classic implementation for a "bootstrap.inc.php" file that stands on 
      // the package directory
      \Loops\Autoloader\Psr40::getInstance()->add( __NAMESPACE__ , __DIR__.'/src' );

      // add another directory to look for
      \Loops\Autoloader\Psr40::getInstance()->add( __NAMESPACE__ , __DIR__.'/tests' );

      // reset all directories to look for
      \Loops\Autoloader\Psr40::getInstance()->set( __NAMESPACE__ , __DIR__.'/dev' );


This class is loosely based on the \Composer\Autoload\ClassLoader.



Contributors:
-------------


- Pierrot Evrard aka Loops — [https://twitter.com/lxxps](https://twitter.com/lxxps)


### Wanna contribute?

There is only one rule to follow: **Challenge yourself**.