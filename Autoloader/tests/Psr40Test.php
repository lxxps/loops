<?php

/*
 * This file is part of the loops/autoloader package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Autoloader;

use Loops\Autoloader\Psr40 as Autoloader;

/**
 * Basic test suite for Autoloading
 *
 * @package    loops/autoloader
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Psr40Test extends \PHPUnit_Framework_TestCase
{
  /**
   * Root directory for dummy packages
   * 
   * @var string
   * @access protected
   */
  public static $_root = null;
    
  /**
   * Array of files to create/remove for dummy packages.
   * 
   * Representation:
   *  path_to_file => array( namespace , class name )
   * 
   * @var array
   * @access protected
   */
  public static $_dummies = array(
      
    // Psr40_Test4/Package_A src
    'Psr40_Test1/Package_A/src/ClassName1.php' => array( 'Psr40_Test1\\Package_A' , 'ClassName1' ) ,
    'Psr40_Test1/Package_A/src/ClassName2.php' => array( 'Psr40_Test1\\Package_A' , 'ClassName2' ) ,
    'Psr40_Test1/Package_A/src/ClassName3.php' => array( 'Psr40_Test1\\Package_A' , 'ClassName3' ) ,
    'Psr40_Test1/Package_A/src/Sub/ClassName1.php' => array( 'Psr40_Test1\\Package_A' , 'Sub_ClassName1' ) ,
    'Psr40_Test1/Package_A/src/Sub/ClassName2.php' => array( 'Psr40_Test1\\Package_A' , 'Sub_ClassName2' ) ,
    'Psr40_Test1/Package_A/src/Sub/ClassName3.php' => array( 'Psr40_Test1\\Package_A' , 'Sub_ClassName3' ) ,
    'Psr40_Test1/Package_A/src/Sub/Sub/ClassName1.php' => array( 'Psr40_Test1\\Package_A' , 'Sub_Sub_ClassName1' ) ,
    'Psr40_Test1/Package_A/src/Sub/Sub/ClassName2.php' => array( 'Psr40_Test1\\Package_A' , 'Sub_Sub_ClassName2' ) ,
    'Psr40_Test1/Package_A/src/Sub/Sub/ClassName3.php' => array( 'Psr40_Test1\\Package_A' , 'Sub_Sub_ClassName3' ) ,
    // Psr40_Test1/Package_B src
    'Psr40_Test1/Package_B/src/ClassName1.php' => array( 'Psr40_Test1\\Package_B' , 'ClassName1' ) ,
    'Psr40_Test1/Package_B/src/ClassName2.php' => array( 'Psr40_Test1\\Package_B' , 'ClassName2' ) ,
    'Psr40_Test1/Package_B/src/ClassName3.php' => array( 'Psr40_Test1\\Package_B' , 'ClassName3' ) ,
    'Psr40_Test1/Package_B/src/Sub/ClassName1.php' => array( 'Psr40_Test1\\Package_B' , 'Sub_ClassName1' ) ,
    'Psr40_Test1/Package_B/src/Sub/ClassName2.php' => array( 'Psr40_Test1\\Package_B' , 'Sub_ClassName2' ) ,
    'Psr40_Test1/Package_B/src/Sub/ClassName3.php' => array( 'Psr40_Test1\\Package_B' , 'Sub_ClassName3' ) ,
    'Psr40_Test1/Package_B/src/Sub/Sub/ClassName1.php' => array( 'Psr40_Test1\\Package_B' , 'Sub_Sub_ClassName1' ) ,
    'Psr40_Test1/Package_B/src/Sub/Sub/ClassName2.php' => array( 'Psr40_Test1\\Package_B' , 'Sub_Sub_ClassName2' ) ,
    'Psr40_Test1/Package_B/src/Sub/Sub/ClassName3.php' => array( 'Psr40_Test1\\Package_B' , 'Sub_Sub_ClassName3' ) ,

    // Psr40_Test2/Package src_A
    'Psr40_Test2/Package/src_A/ClassNameA1.php' => array( 'Psr40_Test2\\Package' , 'ClassNameA1' ) ,
    'Psr40_Test2/Package/src_A/ClassNameA2.php' => array( 'Psr40_Test2\\Package' , 'ClassNameA2' ) ,
    'Psr40_Test2/Package/src_A/ClassNameA3.php' => array( 'Psr40_Test2\\Package' , 'ClassNameA3' ) ,
    'Psr40_Test2/Package/src_A/Sub/ClassNameA1.php' => array( 'Psr40_Test2\\Package' , 'Sub_ClassNameA1' ) ,
    'Psr40_Test2/Package/src_A/Sub/ClassNameA2.php' => array( 'Psr40_Test2\\Package' , 'Sub_ClassNameA2' ) ,
    'Psr40_Test2/Package/src_A/Sub/ClassNameA3.php' => array( 'Psr40_Test2\\Package' , 'Sub_ClassNameA3' ) ,
    'Psr40_Test2/Package/src_A/Sub/Sub/ClassNameA1.php' => array( 'Psr40_Test2\\Package' , 'Sub_Sub_ClassNameA1' ) ,
    'Psr40_Test2/Package/src_A/Sub/Sub/ClassNameA2.php' => array( 'Psr40_Test2\\Package' , 'Sub_Sub_ClassNameA2' ) ,
    'Psr40_Test2/Package/src_A/Sub/Sub/ClassNameA3.php' => array( 'Psr40_Test2\\Package' , 'Sub_Sub_ClassNameA3' ) ,
    // Psr40_Test2/Package src_B
    'Psr40_Test2/Package/src_B/ClassNameB1.php' => array( 'Psr40_Test2\\Package' , 'ClassNameB1' ) ,
    'Psr40_Test2/Package/src_B/ClassNameB2.php' => array( 'Psr40_Test2\\Package' , 'ClassNameB2' ) ,
    'Psr40_Test2/Package/src_B/ClassNameB3.php' => array( 'Psr40_Test2\\Package' , 'ClassNameB3' ) ,
    'Psr40_Test2/Package/src_B/Sub/ClassNameB1.php' => array( 'Psr40_Test2\\Package' , 'Sub_ClassNameB1' ) ,
    'Psr40_Test2/Package/src_B/Sub/ClassNameB2.php' => array( 'Psr40_Test2\\Package' , 'Sub_ClassNameB2' ) ,
    'Psr40_Test2/Package/src_B/Sub/ClassNameB3.php' => array( 'Psr40_Test2\\Package' , 'Sub_ClassNameB3' ) ,
    'Psr40_Test2/Package/src_B/Sub/Sub/ClassNameB1.php' => array( 'Psr40_Test2\\Package' , 'Sub_Sub_ClassNameB1' ) ,
    'Psr40_Test2/Package/src_B/Sub/Sub/ClassNameB2.php' => array( 'Psr40_Test2\\Package' , 'Sub_Sub_ClassNameB2' ) ,
    'Psr40_Test2/Package/src_B/Sub/Sub/ClassNameB3.php' => array( 'Psr40_Test2\\Package' , 'Sub_Sub_ClassNameB3' ) ,

    // Psr40_Test3/Package src_A
    'Psr40_Test3/Package/src_A/ClassNameA1.php' => array( 'Psr40_Test3\\Package' , 'ClassNameA1' ) ,
    'Psr40_Test3/Package/src_A/ClassNameA2.php' => array( 'Psr40_Test3\\Package' , 'ClassNameA2' ) ,
    'Psr40_Test3/Package/src_A/ClassNameA3.php' => array( 'Psr40_Test3\\Package' , 'ClassNameA3' ) ,
    'Psr40_Test3/Package/src_A/Sub/ClassNameA1.php' => array( 'Psr40_Test3\\Package' , 'Sub_ClassNameA1' ) ,
    'Psr40_Test3/Package/src_A/Sub/ClassNameA2.php' => array( 'Psr40_Test3\\Package' , 'Sub_ClassNameA2' ) ,
    'Psr40_Test3/Package/src_A/Sub/ClassNameA3.php' => array( 'Psr40_Test3\\Package' , 'Sub_ClassNameA3' ) ,
    'Psr40_Test3/Package/src_A/Sub/Sub/ClassNameA1.php' => array( 'Psr40_Test3\\Package' , 'Sub_Sub_ClassNameA1' ) ,
    'Psr40_Test3/Package/src_A/Sub/Sub/ClassNameA2.php' => array( 'Psr40_Test3\\Package' , 'Sub_Sub_ClassNameA2' ) ,
    'Psr40_Test3/Package/src_A/Sub/Sub/ClassNameA3.php' => array( 'Psr40_Test3\\Package' , 'Sub_Sub_ClassNameA3' ) ,
    // Psr40_Test3/Package src_B
    'Psr40_Test3/Package/src_B/ClassNameB1.php' => array( 'Psr40_Test3\\Package' , 'ClassNameB1' ) ,
    'Psr40_Test3/Package/src_B/ClassNameB2.php' => array( 'Psr40_Test3\\Package' , 'ClassNameB2' ) ,
    'Psr40_Test3/Package/src_B/ClassNameB3.php' => array( 'Psr40_Test3\\Package' , 'ClassNameB3' ) ,
    'Psr40_Test3/Package/src_B/Sub/ClassNameB1.php' => array( 'Psr40_Test3\\Package' , 'Sub_ClassNameB1' ) ,
    'Psr40_Test3/Package/src_B/Sub/ClassNameB2.php' => array( 'Psr40_Test3\\Package' , 'Sub_ClassNameB2' ) ,
    'Psr40_Test3/Package/src_B/Sub/ClassNameB3.php' => array( 'Psr40_Test3\\Package' , 'Sub_ClassNameB3' ) ,
    'Psr40_Test3/Package/src_B/Sub/Sub/ClassNameB1.php' => array( 'Psr40_Test3\\Package' , 'Sub_Sub_ClassNameB1' ) ,
    'Psr40_Test3/Package/src_B/Sub/Sub/ClassNameB2.php' => array( 'Psr40_Test3\\Package' , 'Sub_Sub_ClassNameB2' ) ,
    'Psr40_Test3/Package/src_B/Sub/Sub/ClassNameB3.php' => array( 'Psr40_Test3\\Package' , 'Sub_Sub_ClassNameB3' ) ,

    // Psr40_Test4/Package src
    'Psr40_Test4/Package/src/ClassName1.php' => array( 'Psr40_Test4\\Package' , 'ClassName1' ) ,
    'Psr40_Test4/Package/src/ClassName2.php' => array( 'Psr40_Test4\\Package' , 'ClassName2' ) ,
    'Psr40_Test4/Package/src/ClassName3.php' => array( 'Psr40_Test4\\Package' , 'ClassName3' ) ,
    'Psr40_Test4/Package/src/Sub/ClassName1.php' => array( 'Psr40_Test4\\Package' , 'Sub_ClassName1' ) ,
    'Psr40_Test4/Package/src/Sub/ClassName2.php' => array( 'Psr40_Test4\\Package' , 'Sub_ClassName2' ) ,
    'Psr40_Test4/Package/src/Sub/ClassName3.php' => array( 'Psr40_Test4\\Package' , 'Sub_ClassName3' ) ,
    'Psr40_Test4/Package/src/Sub/Sub/ClassName1.php' => array( 'Psr40_Test4\\Package' , 'Sub_Sub_ClassName1' ) ,
    'Psr40_Test4/Package/src/Sub/Sub/ClassName2.php' => array( 'Psr40_Test4\\Package' , 'Sub_Sub_ClassName2' ) ,
    'Psr40_Test4/Package/src/Sub/Sub/ClassName3.php' => array( 'Psr40_Test4\\Package' , 'Sub_Sub_ClassName3' ) ,

    // Psr40_Test5/Package src
    'Psr40_Test5/Package/src/ClassName1.php' => array( 'Psr40_Test5\\Package' , 'ClassName1' ) ,
    'Psr40_Test5/Package/src/ClassName2.php' => array( 'Psr40_Test5\\Package' , 'ClassName2' ) ,
    'Psr40_Test5/Package/src/ClassName3.php' => array( 'Psr40_Test5\\Package' , 'ClassName3' ) ,
    'Psr40_Test5/Package/src/Sub/ClassName1.php' => array( 'Psr40_Test5\\Package' , 'Sub_ClassName1' ) ,
    'Psr40_Test5/Package/src/Sub/ClassName2.php' => array( 'Psr40_Test5\\Package' , 'Sub_ClassName2' ) ,
    'Psr40_Test5/Package/src/Sub/ClassName3.php' => array( 'Psr40_Test5\\Package' , 'Sub_ClassName3' ) ,
    'Psr40_Test5/Package/src/Sub/Sub/ClassName1.php' => array( 'Psr40_Test5\\Package' , 'Sub_Sub_ClassName1' ) ,
    'Psr40_Test5/Package/src/Sub/Sub/ClassName2.php' => array( 'Psr40_Test5\\Package' , 'Sub_Sub_ClassName2' ) ,
    'Psr40_Test5/Package/src/Sub/Sub/ClassName3.php' => array( 'Psr40_Test5\\Package' , 'Sub_Sub_ClassName3' ) ,
    
  );
  
  /**
   * Create dummy packages structure
   * 
   * @param  none
   * @return void
   * @throws InvalidArgumentException
   */
  public static function setUpBeforeClass()
  {
    static::$_root = sys_get_temp_dir();
    isset( $_ENV['fixtures_dir'] ) and ( static::$_root = $_ENV['fixtures_dir'] );
    
    // if the directory is not writeable, attemp to create the directory
    if( ( ! is_writeable( static::$_root ) ) && ( ! @mkdir( static::$_root , 0777 ) ) )
    {
      // we would like to stop the entire test case, but it is not possible
      throw new \RuntimeException( sprintf( 'Directory "%s" is not writeable. Cannot create dummy packages files.' , static::$_root ) );
    }
    
    foreach( static::$_dummies as $file => $data )
    {
      $file = static::$_root.'/'.$file;
      
      list( $namespace , $classname ) = $data;
      
      $content = '<?php namespace '.$namespace.'; class '.$classname.' {}';
      
      // write at first
      if( ! @ file_put_contents( $file , $content ) )
      {
        // failed, create directory
        mkdir( dirname( $file ) , 0777 , true );
        // then rewrite
        file_put_contents( $file , $content );
        // we cannot failed again...
      }
    }
  }

  /**
   * Remove dummy packages structure, except root directory (never know)
   * 
   * @param  none
   * @return void
   */
  public static function tearDownAfterClass()
  {
    $root = static::$_root;
    
    // remove all files and directories
    foreach( array_keys( static::$_dummies ) as $file )
    {
      unlink( $root.'/'.$file );
      
      // now attemp to remove all directories
      
      $dirs = explode( '/' , $file );
      
      do
      {
        // pop a directory (or filename at first run)
        array_pop( $dirs );
        
      } while( $dirs && @rmdir( $root.'/'.implode( '/' , $dirs ) ) ); // if we fail to remove the directory, it may be not empty yet
      
    }
  }
  
  /*
   * Test case 1: A/B package
   */
  public function test1()
  {
    // helping POOP, singleton usage is not mandatory
    $autoloader = new Autoloader();
    
    // register package A
    $autoloader->add( 'Psr40_Test1\\Package_A' , static::$_root.'/Psr40_Test1/Package_A/src' );
    
    // register package B
    $autoloader->add( 'Psr40_Test1\\Package_B' , static::$_root.'/Psr40_Test1/Package_B/src' );
    
    // test existing class
    
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_A\\ClassName1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_A\\Sub_ClassName1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_A\\Sub_Sub_ClassName1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_A\\ClassName2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_A\\Sub_ClassName2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_A\\Sub_Sub_ClassName2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_A\\ClassName3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_A\\Sub_ClassName3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_A\\Sub_Sub_ClassName3' , true ) );
    
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_B\\ClassName1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_B\\Sub_ClassName1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_B\\Sub_Sub_ClassName1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_B\\ClassName2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_B\\Sub_ClassName2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_B\\Sub_Sub_ClassName2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_B\\ClassName3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_B\\Sub_ClassName3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test1\\Package_B\\Sub_Sub_ClassName3' , true ) );
    
    // test unexisting class
    
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_A\\ClassName1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_A\\Sub_ClassName1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_A\\Sub_Sub_ClassName1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_A\\ClassName2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_A\\Sub_ClassName2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_A\\Sub_Sub_ClassName2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_A\\ClassName3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_A\\Sub_ClassName3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_A\\Sub_Sub_ClassName3X' , true ) );
    
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_B\\ClassName1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_B\\Sub_ClassName1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_B\\Sub_Sub_ClassName1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_B\\ClassName2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_B\\Sub_ClassName2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_B\\Sub_Sub_ClassName2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_B\\ClassName3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_B\\Sub_ClassName3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test1\\Package_B\\Sub_Sub_ClassName3X' , true ) );
    
  }
  
  /*
   * Test case 2: A/B source
   */
  public function test2()
  {
    // helping POOP, singleton usage is not mandatory
    $autoloader = new Autoloader();
    
    // register source A
    $autoloader->add( 'Psr40_Test2\\Package' , static::$_root.'/Psr40_Test2/Package/src_A' );
    
    // register source B
    $autoloader->add( 'Psr40_Test2\\Package' , static::$_root.'/Psr40_Test2/Package/src_B' );
    
    // test existing class
    
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\ClassNameA1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\Sub_ClassNameA1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\Sub_Sub_ClassNameA1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\ClassNameA2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\Sub_ClassNameA2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\Sub_Sub_ClassNameA2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\ClassNameA3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\Sub_ClassNameA3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\Sub_Sub_ClassNameA3' , true ) );
    
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\ClassNameB1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\Sub_ClassNameB1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\Sub_Sub_ClassNameB1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\ClassNameB2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\Sub_ClassNameB2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\Sub_Sub_ClassNameB2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\ClassNameB3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\Sub_ClassNameB3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test2\\Package\\Sub_Sub_ClassNameB3' , true ) );
    
    // test unexisting class
    
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\ClassNameA1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\Sub_ClassNameA1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\Sub_Sub_ClassNameA1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\ClassNameA2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\Sub_ClassNameA2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\Sub_Sub_ClassNameA2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\ClassNameA3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\Sub_ClassNameA3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\Sub_Sub_ClassNameA3X' , true ) );
    
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\ClassNameB1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\Sub_ClassNameB1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\Sub_Sub_ClassNameB1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\ClassNameB2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\Sub_ClassNameB2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\Sub_Sub_ClassNameB2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\ClassNameB3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\Sub_ClassNameB3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test2\\Package\\Sub_Sub_ClassNameB3X' , true ) );
    
  }
  
  /*
   * Test case 3: A/B source when A or B is removed
   */
  public function test3()
  {
    // helping POOP, singleton usage is not mandatory
    $autoloader = new Autoloader();
    
    // register source A and B
    $autoloader->add( 'Psr40_Test3\\Package' , array( 
      static::$_root.'/Psr40_Test3/Package/src_A' ,
      static::$_root.'/Psr40_Test3/Package/src_B' ,
    ) );
    
    // test existing class
    
    $this->assertTrue( class_exists( 'Psr40_Test3\\Package\\ClassNameA1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test3\\Package\\Sub_ClassNameA1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test3\\Package\\Sub_Sub_ClassNameA1' , true ) );
    
    $this->assertTrue( class_exists( 'Psr40_Test3\\Package\\ClassNameB1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test3\\Package\\Sub_ClassNameB1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test3\\Package\\Sub_Sub_ClassNameB1' , true ) );
    
    // test unexisting class
    
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\ClassNameA1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_ClassNameA1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_Sub_ClassNameA1X' , true ) );
    
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\ClassNameB1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_ClassNameB1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_Sub_ClassNameB1X' , true ) );
    
    
    // set only source A, will remove source B
    $autoloader->set( 'Psr40_Test3\\Package' , array( 
      static::$_root.'/Psr40_Test3/Package/src_A' ,
    ) );
    
    // test existing class
    
    $this->assertTrue( class_exists( 'Psr40_Test3\\Package\\ClassNameA2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test3\\Package\\Sub_ClassNameA2' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test3\\Package\\Sub_Sub_ClassNameA2' , true ) );
    
    // test existing class that cannot be loaded anymore
    
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\ClassNameB2' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_ClassNameB2' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_Sub_ClassNameB2' , true ) );
    
    // test unexisting class
    
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\ClassNameA2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_ClassNameA2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_Sub_ClassNameA2X' , true ) );
    
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\ClassNameB2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_ClassNameB2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_Sub_ClassNameB2X' , true ) );
    
    
    // set only source B, will remove source A
    $autoloader->set( 'Psr40_Test3\\Package' , array( 
      static::$_root.'/Psr40_Test3/Package/src_B' ,
    ) );
    
    // test existing class that cannot be loaded anymore
    
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\ClassNameA3' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_ClassNameA3' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_Sub_ClassNameA3' , true ) );
    
    // test existing class
    
    $this->assertTrue( class_exists( 'Psr40_Test3\\Package\\ClassNameB3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test3\\Package\\Sub_ClassNameB3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test3\\Package\\Sub_Sub_ClassNameB3' , true ) );
    
    // test unexisting class
    
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\ClassNameA3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_ClassNameA3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_Sub_ClassNameA3X' , true ) );
    
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\ClassNameB3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_ClassNameB3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test3\\Package\\Sub_Sub_ClassNameB3X' , true ) );
    
  }
  
  /*
   * Test case 4: unregister package
   */
  public function test4()
  {
    // helping POOP, singleton usage is not mandatory
    $autoloader = new Autoloader();
    
    // register package
    $autoloader->add( 'Psr40_Test4\\Package' , static::$_root.'/Psr40_Test4/Package/src' );
    
    // test existing class
    
    $this->assertTrue( class_exists( 'Psr40_Test4\\Package\\ClassName1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test4\\Package\\Sub_ClassName1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test4\\Package\\Sub_Sub_ClassName1' , true ) );
    
    // test unexisting class
    
    $this->assertFalse( class_exists( 'Psr40_Test4\\Package\\ClassName1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test4\\Package\\Sub_ClassName1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test4\\Package\\Sub_Sub_ClassName1X' , true ) );
    
    
    // unregister package
    $autoloader->remove( 'Psr40_Test4\\Package' );
    
    // test existing class that cannot be loaded anymore
    
    $this->assertFalse( class_exists( 'Psr40_Test4\\Package\\ClassName2' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test4\\Package\\Sub_ClassName2' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test4\\Package\\Sub_Sub_ClassName2' , true ) );
    
    // test unexisting class
    
    $this->assertFalse( class_exists( 'Psr40_Test4\\Package\\ClassName2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test4\\Package\\Sub_ClassName2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test4\\Package\\Sub_Sub_ClassName2X' , true ) );
    
    
    // register package
    $autoloader->add( 'Psr40_Test4\\Package' , static::$_root.'/Psr40_Test4/Package/src' );
    
    // test existing class
    
    $this->assertTrue( class_exists( 'Psr40_Test4\\Package\\ClassName3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test4\\Package\\Sub_ClassName3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test4\\Package\\Sub_Sub_ClassName3' , true ) );
    
    // test unexisting class
    
    $this->assertFalse( class_exists( 'Psr40_Test4\\Package\\ClassName3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test4\\Package\\Sub_ClassName3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test4\\Package\\Sub_Sub_ClassName3X' , true ) );
    
  }
  
  /*
   * Test case 5: unregister autoloader
   */
  public function test5()
  {
    // helping POOP, singleton usage is not mandatory
    $autoloader = new Autoloader();
    
    // register package
    $autoloader->add( 'Psr40_Test5\\Package' , static::$_root.'/Psr40_Test5/Package/src' );
    
    // test existing class
    
    $this->assertTrue( class_exists( 'Psr40_Test5\\Package\\ClassName1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test5\\Package\\Sub_ClassName1' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test5\\Package\\Sub_Sub_ClassName1' , true ) );
    
    // test unexisting class
    
    $this->assertFalse( class_exists( 'Psr40_Test5\\Package\\ClassName1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test5\\Package\\Sub_ClassName1X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test5\\Package\\Sub_Sub_ClassName1X' , true ) );
    
    // because of garbage collector, we cannot unset autoloader and expect that
    // the destructor will be called immediately: only PHP has control of the 
    // garbage collector
    // ie. unset( $autoloader ); will not destroy the object
    
    // unregister autoloader
    $autoloader->unregister();
    
    // test existing class that cannot be loaded anymore
    
    $this->assertFalse( class_exists( 'Psr40_Test5\\Package\\ClassName2' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test5\\Package\\Sub_ClassName2' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test5\\Package\\Sub_Sub_ClassName2' , true ) );
    
    // test unexisting class
    
    $this->assertFalse( class_exists( 'Psr40_Test5\\Package\\ClassName2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test5\\Package\\Sub_ClassName2X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test5\\Package\\Sub_Sub_ClassName2X' , true ) );
    
    
    // register autoloader
    $autoloader->register();
    
    // test existing class
    
    $this->assertTrue( class_exists( 'Psr40_Test5\\Package\\ClassName3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test5\\Package\\Sub_ClassName3' , true ) );
    $this->assertTrue( class_exists( 'Psr40_Test5\\Package\\Sub_Sub_ClassName3' , true ) );
    
    // test unexisting class
    
    $this->assertFalse( class_exists( 'Psr40_Test5\\Package\\ClassName3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test5\\Package\\Sub_ClassName3X' , true ) );
    $this->assertFalse( class_exists( 'Psr40_Test5\\Package\\Sub_Sub_ClassName3X' , true ) );
    
  }
  
}
