<?php

/*
 * This file is part of the loops/autoloader package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Autoloader;

/**
 * Class to register autoloading function for loops/* package.
 * 
 * loops/* packages use a custom autoloader that mix PSR-4 for package-oriented 
 * autoloading, and PSR-0 for class name underscore replacement.
 * 
 * You should never need another \Loops\Autoloader\Psr40 instance, so it is 
 * singleton.
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    loops/autoloader
 * @author     Loops <pierrotevrard@gmail.com>
 */
class Psr40
{
  /**
   * Current \Loops\Autoloader\Psr40 instance.
   *
   * @var \Loops\Autoloader\Psr40 Current instance
   * @access protected
   * @static
   */
  public static $_instance;
  
  /**
   * Table for prefixes by directory.
   * 
   * According to \Composer\Autoload\ClassLoader, aach prefix may have multiple 
   * directories where to look for the class.
   * 
   * @var string
   * @access protected
   */
  public $_psr40_dir = array();
  
  /**
   * Table for prefixes by length.
   * 
   * According to \Composer\Autoload\ClassLoader, in order to reduce loop 
   * process, first keys are the first letter of the prefix.
   * 
   * @var string
   * @access protected
   */
  public $_psr40_len = array();
  
  /**
   * Method to retrieve \Loops\Autoloader\Psr40 instance.
   *
   * @param none
   * @return \Loops\Autoloader\Psr40 Current instance
   * @access public
   * @static
   */ 
  public static function getInstance()
  {
    static::$_instance or ( static::$_instance = new static() );
    
    return static::$_instance;
  }
  
  /**
   * \Loops\Autoloader\Psr40 constructor.
   * Protected constructor to force use of getInstance() static method.
   * 
   * Keep in mind POOP pattern: the constructor should be protected, 
   * but it is not.
   *
   * @param  none
   * @return void
   * @access protected
   */
  public function __construct()
  {
    $this->register();
  }
  
  /**
   * \Loops\Autoloader\Psr40 destructor.
   * 
   * Unregister autoloading seems a good idea, not?
   *
   * @param  none
   * @return void
   * @access protected
   */
  public function __destruct()
  {
    // do not care if the autoloader has not been registered
    $this->unregister();
  }
  
  /**
   * Set a relation beetween prefix and directories
   *
   * @param  string        $prefix
   * @param  string|array  $directories
   * @return void
   * @access public
   */
  public function set( $prefix , $dirs )
  {
    // sanitize prefix, necessary to avoid unexpected collision
    substr( $prefix , -1 ) === '\\' or ( $prefix .= '\\' );
    
    $this->_psr40_len[$prefix{0}][$prefix] = strlen( $prefix );
    $this->_psr40_dir[$prefix] = array_values( (array)$dirs ); // force keys as integer
  }
  
  /**
   * Add a relation beetween a prefix and directories
   *
   * @param  string        $prefix
   * @param  string|array  $directories
   * @param  [bool]        $prepend
   * @return void
   * @access public
   */
  public function add( $prefix , $dirs , $prepend = false )
  {
    // sanitize prefix, necessary to avoid unexpected collision
    substr( $prefix , -1 ) === '\\' or ( $prefix .= '\\' );
    
    if( isset($this->_psr40_dir[$prefix]) )
    {
      if( $prepend )
      {
        $this->_psr40_dir[$prefix] = array_merge( array_values( (array)$dirs ) , $this->_psr40_dir[$prefix] ); // force keys as integer
      }
      else
      {
        $this->_psr40_dir[$prefix] = array_merge( $this->_psr40_dir[$prefix] , array_values( (array)$dirs ) ); // force keys as integer
      }
    }
    else
    {
      // do set
      $this->set( $prefix , $dirs );
    }
  }
  
  /**
   * Get all directories for a prefix.
   *
   * @param  string        $prefix
   * @return array|null
   * @access public
   */
  public function get( $prefix )
  {
    // sanitize prefix, to be compliant with set/add methods
    substr( $prefix , -1 ) === '\\' or ( $prefix .= '\\' );
    
    print PHP_EOL.$prefix.PHP_EOL;
    
    if( isset($this->_psr40_dir[$prefix]) )
    {
      return $this->_psr40_dir[$prefix];
    }
    else
    {
      return null; 
    }
  }
  
  /**
   * Remove a prefix.
   *
   * @param  string        $prefix
   * @return void
   * @access public
   */
  public function remove( $prefix )
  {
    // sanitize prefix, to be compliant with set/add methods
    substr( $prefix , -1 ) === '\\' or ( $prefix .= '\\' );
    
    unset( $this->_psr40_len[$prefix{0}][$prefix] , $this->_psr40_dir[$prefix] );
  }
  
  /**
   * Register autoloading.
   *
   * @param none
   * @return bool
   * @access public
   */
  public function register()
  {
    return spl_autoload_register( array( $this , '_autoload' ) , true , false ); // do not prepend
  }

  /**
   * Unregisters autoloading.
   *
   * @param none
   * @return bool
   * @access public
   */
  public function unregister()
  {
    return spl_autoload_unregister( array( $this , '_autoload' ) );
  }
  
  /**
   * Dump all registered prefixes to directories.
   *
   * @param  none
   * @return array Prefixes to directories array
   * @access public
   */
  public function dump()
  {
    return $this->_psr40_dir;
  }
  
  /**
   * Method to autoload class.
   * 
   * This autoloading method does not allow not package-oriented structure.
   * 
   * Also, following PSR-4 recommandations, autoloader implementations MUST NOT 
   * throw exceptions, MUST NOT raise errors of any level, and SHOULD NOT return 
   * a value.
   * 
   * @see http://www.php-fig.org/psr/psr-4/
   * 
   *
   * @param string $class Class name
   * @param string $ext   Extension to look for
   * @return mixed
   * @access protected
   */
  public function _autoload( $class , $ext = '.php' )
  {
    $first = $class{0};
    
    if( isset($this->_psr40_len[$first]) )
    {
      foreach( $this->_psr40_len[$first] as $prefix => $len )
      {
        if( strpos( $class , $prefix ) === 0 )
        {
          $i = 0;
          $base = DIRECTORY_SEPARATOR.str_replace( array( '\\' , '_' ) , DIRECTORY_SEPARATOR , substr( $class , $len ) ).$ext;
          $dirs = $this->_psr40_dir[$prefix]; // shortcut
          
          do
          {
            $file = $dirs[$i].$base;
            
            // file_exists() is our best bet
            if( file_exists( $file ) )
            {
              // do not care if $this/self is accessed from the included file
              // developer may have a good reason to do it
              
              return require $file; // stop right now
            }
            
            // continue for next directory
          } while( isset($dirs[++$i]) ); // pre-increment
          
          // whatever, do not continue the loop on all available prefixes
          break;
        }
      }
    }
    
    // not found in hash table but...
    
    // HHVM environment
    if( defined( '\\HHVM_VERSION' ) && $ext !== '.hh' ) // avoid infinite loop
    {
      // recall with another extension
      return $this->_autoload( $class , '.hh' );
    }
    
    // workaround for PHP 5.3.0 - 5.3.2 https://bugs.php.net/50731
    if( $first === '\\' )
    {
      // recall with one less letter
      return $this->_autoload( substr( $class , 1 ) );
    }
    
    // ok, really not found
    return false;
  }

}